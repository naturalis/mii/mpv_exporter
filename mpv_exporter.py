#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from prometheus_client import start_http_server, Summary
from prometheus_client.core import Counter, GaugeMetricFamily, CounterMetricFamily, REGISTRY
import time
import socket
import json
import sys
import os

class MpvCollector(object):
    def collect(self):
        s = MpvSocket()
        s.send('{ "command": ["get_property", "frame-drop-count"] }')
        v = s.receive()["data"]
        yield CounterMetricFamily('mpv_frames_dropped_vo_total',
                      'Total amount of frames dropped by VO during playback of current file by mpv',
                      value=v)
        s.send('{ "command": ["get_property", "decoder-frame-drop-count"] }')
        v = s.receive()["data"]
        yield CounterMetricFamily('mpv_frames_dropped_decoder_total',
                      'Total amount of frames dropped by decoder during playback of current file by mpv',
                      value=v)
        s.send('{ "command": ["get_property", "vo-delayed-frame-count"] }')
        v = s.receive()["data"]
        yield CounterMetricFamily('mpv_frames_delayed_total',
                      'Total amount of frames delayed during the current run of mpv',
                      value=v)
        s.send('{ "command": ["get_property", "volume"] }')
        v = s.receive()["data"]
        yield GaugeMetricFamily('mpv_volume_level',
                      'Volume level of mpv',
                      value=v)
        s.send('{ "command": ["get_property", "ao-volume"] }')
        v = s.receive()["data"]
        yield GaugeMetricFamily('mpv_systemvolume_level',
                      'System level',
                      value=v)
        s.send('{ "command": ["get_property", "avsync"] }')
        v = s.receive()["data"]
        yield GaugeMetricFamily('mpv_avsync_seconds',
                      'A/V synchronization difference in seconds',
                      value=v)
        s.send('{ "command": ["get_property", "total-avsync-change"] }')
        v = s.receive()["data"]
        yield GaugeMetricFamily('mpv_avsyncchange_seconds',
                      'Total A-V sync correction done',
                      value=v)
        s.close()

class MpvSocket():
    """
    Start a socket object for communication with mpv's JSON IPC
    """

    def __init__(self, sock=None):
        if sock is None:
            self.sock = socket.socket(
                            socket.AF_UNIX, socket.SOCK_STREAM)
            self.sock.connect(socket_file)
        else:
            self.sock = sock

    def send(self, msg):
        msg = (msg + '\n').encode()
        sent = self.sock.send(msg)
        if sent == 0:
            raise RuntimeError("Socket connection broken")

    def receive(self):
        f = self.sock.makefile ('r')
        while 1:
            # Use readline() to avoid buffering problems
            line = f.readline()
            if line == "":
                break
            try:
                data = json.loads(line)
            except:
                raise Error("ERROR")
            return data

    def close(self):
        self.sock.close()


if __name__ == '__main__':
    try:
        socket_file = os.environ['MPV_SOCKET']
    except Exception:
        print("Please set the MPV_SOCKET environment variable.")
        sys.exit(1)
    # Start up the server to expose the metrics.
    start_http_server(9491)
    # Register MpvCollector
    REGISTRY.register(MpvCollector())
    while True:
        #process_request(random.random())
        time.sleep(5)
        c = MpvCollector()
        c.collect()
